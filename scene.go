package main

type scene interface {
	init()
	draw()
	update()
}

type scene1 struct {
	scene
	_board board
}

func (s *scene1) init() {
	s._board = board{rows: 20, columns: 10}
	s._board.init()
}

func (s *scene1) draw() {
	s._board.draw()
}

func (s *scene1) update() {
	s._board.update()
}

type scene2 struct {
	scene
	_board board
}

func (s *scene2) init() {
	s._board = board{rows: 20, columns: 3}
	s._board.init()
}

func (s *scene2) draw() {
	s._board.draw()
}

func (s *scene2) update() {
	s._board.update()
}
