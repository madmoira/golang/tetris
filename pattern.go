package main

import (
	"container/ring"
	"fmt"
)

// Pattern structure, for the moment it only contains the valid structures
type Pattern struct {
	patterns *ring.Ring
	size     int
}

// Patterns stores all the available patterns
var Patterns []Pattern

var pattern1 Pattern
var pattern2 Pattern
var pattern3 Pattern

func init() {

	fmt.Println("Initializing patterns")
	patterns1 := [][][]int{
		{
			{0, 0, 0},
			{0, 1, 1},
			{0, 0, 0},
		},
		{
			{0, 0, 0},
			{0, 1, 0},
			{0, 1, 0},
		},
		{
			{0, 0, 0},
			{1, 1, 0},
			{0, 0, 0},
		},
		{
			{0, 1, 0},
			{0, 1, 0},
			{0, 0, 0},
		},
	}

	patterns2 := [][][]int{
		{
			{0, 0, 0},
			{1, 1, 1},
			{0, 0, 1},
		},
		{
			{0, 1, 0},
			{0, 1, 0},
			{1, 1, 0},
		},
		{
			{1, 0, 0},
			{1, 1, 1},
			{0, 0, 0},
		},
		{
			{0, 1, 1},
			{0, 1, 0},
			{0, 1, 0},
		},
	}

	patterns3 := [][][]int{
		{
			{1, 1, 1},
			{0, 1, 0},
			{0, 0, 0},
		},
		{
			{0, 0, 1},
			{0, 1, 1},
			{0, 0, 1},
		},
		{
			{0, 0, 0},
			{0, 1, 0},
			{1, 1, 1},
		},
		{
			{1, 0, 0},
			{1, 1, 0},
			{1, 0, 0},
		},
	}

	pattern1 = Pattern{patterns: ring.New(4), size: 3}
	pattern2 = Pattern{patterns: ring.New(4), size: 3}
	pattern3 = Pattern{patterns: ring.New(4), size: 3}

	for i := 0; i < 4; i++ {
		pattern1.patterns.Value = patterns1[i]
		pattern1.patterns = pattern1.patterns.Next()
		pattern2.patterns.Value = patterns2[i]
		pattern2.patterns = pattern2.patterns.Next()
		pattern3.patterns.Value = patterns3[i]
		pattern3.patterns = pattern3.patterns.Next()
	}

	Patterns = []Pattern{pattern1, pattern2, pattern3}
}
