package main

import (
	"math/rand"
	"time"

	r "github.com/lachee/raylib-goplus/raylib"
)

func main() {
	sm := sceneManager{}
	s1 := scene1{}
	s2 := scene2{}
	sm.AvailableScenes = append(sm.AvailableScenes, &s1)
	sm.AvailableScenes = append(sm.AvailableScenes, &s2)
	sm.setScene(0)

	rand.Seed(time.Now().UnixNano())

	r.InitWindow(800, 450, "Raylib Go Plus")
	r.SetTargetFPS(60)

	for !r.WindowShouldClose() {
		r.BeginDrawing()
		r.ClearBackground(r.RayWhite)
		sm.update()
		sm.draw()
		r.EndDrawing()
	}
	r.CloseWindow()
}
