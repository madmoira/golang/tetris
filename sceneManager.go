package main

type sceneManager struct {
	CurrentScene    scene
	AvailableScenes []scene
}

func (m *sceneManager) setScene(index int) {
	m.CurrentScene = m.AvailableScenes[index]
	m.CurrentScene.init()
}

func (m *sceneManager) update() {
	m.CurrentScene.update()
}

func (m *sceneManager) draw() {
	m.CurrentScene.draw()
}
