package main

import (
	r "github.com/lachee/raylib-goplus/raylib"
)

type board struct {
	rows             int
	columns          int
	matrix           [][]int
	currentPiece     piece
	nextMovementTime float32
	currentTime      float32
}

func (b *board) init() {
	b.currentTime = 0
	b.nextMovementTime = 0
	b.currentPiece = piece{
		rows:            b.rows,
		columns:         b.columns,
		currentPosition: newVector2(2, 1),
	}
	b.currentPiece.init()

	for i := 0; i < b.columns; i++ {
		b.matrix = append(b.matrix, make([]int, b.rows))
	}

	b.updatePiecePosition()
}

func (b *board) updatePiecePosition() {
	b.currentPiece.drawPieceInBoard(b)
}

func (b *board) update() {
	b.currentTime += r.GetFrameTime()

	if b.currentTime > b.nextMovementTime {
		b.currentPiece.movePiece(0, 1)
		b.nextMovementTime += 0.5
		b.updatePiecePosition()
	}

	if r.IsKeyPressed(r.KeyA) {
		b.currentPiece.movePiece(-1, 0)
		b.updatePiecePosition()
	}

	if r.IsKeyPressed(r.KeyD) {
		b.currentPiece.movePiece(1, 0)
		b.updatePiecePosition()
	}

	if r.IsKeyPressed(r.KeyW) {
		b.currentPiece.rotate()
		b.updatePiecePosition()
	}
}

func (b *board) draw() {
	for i := range b.matrix {
		for j := range b.matrix[i] {
			if b.matrix[i][j] == 0 {
				r.DrawRectangle(20*i, 20*j, 19, 19, r.GopherBlue)
			} else if b.matrix[i][j] == 1 {
				r.DrawRectangle(20*i, 20*j, 19, 19, r.Red)
			}
		}
	}
}
