package main

type vector2 struct {
	X int
	Y int
}

func newVector2(x int, y int) vector2 {
	return vector2{X: x, Y: y}
}
