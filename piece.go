package main

import (
	"fmt"
	"math/rand"
	"time"
)

type piece struct {
	rows             int
	columns          int
	previousPosition vector2
	currentPosition  vector2
	pattern          Pattern
}

func (p *piece) init() {
	rand.Seed(time.Now().UnixNano())
	idx := rand.Intn(len(Patterns))
	fmt.Println("Selecting a new pattern", idx, len(Patterns))
	p.pattern = Patterns[idx]
}

func (p *piece) storePosition() {
	p.previousPosition.X = p.currentPosition.X
	p.previousPosition.Y = p.currentPosition.Y
}

func (p *piece) drawPieceInBoard(b *board) {
	for i := range b.matrix {
		for j := range b.matrix[i] {
			if b.matrix[i][j] == 1 {
				b.matrix[i][j] = 0
			}
		}
	}

	currentPattern, _ := p.pattern.patterns.Value.([][]int)
	for i := -1; i < len(currentPattern)-1; i++ {
		for j := -1; j < len(currentPattern[i+1])-1; j++ {
			b.matrix[p.currentPosition.X+i][p.currentPosition.Y+j] = currentPattern[i+1][j+1]
		}
	}
}

func (p *piece) rotate() {
	fmt.Println("Piece rotated")
	p.pattern.patterns = p.pattern.patterns.Next()
}

func (p *piece) allowMovement(x int, y int) bool {
	if x < 0 && p.currentPosition.X+x < 0 {
		return false
	} else if x > 0 && p.currentPosition.X+x > p.columns-1 {
		return false
	} else if y > 0 && p.currentPosition.Y+y > p.rows-1 {
		return false
	}

	return true
}

func (p *piece) movePiece(x int, y int) {
	if !p.allowMovement(x, y) {
		return
	}
	p.storePosition()
	p.currentPosition.X += x
	p.currentPosition.Y += y
}
